OS	:=	$(shell	uname)

docker-build:
	@docker-compose build

docker-up:
ifeq	($(OS),Darwin)
				@docker	volume	create	--name=php-sync
				@docker-sync	start
				@docker-compose	-f	docker-compose.yml	-f	docker-compose-macos.yml	up	-d
else
				export USER_ID=$(id -u)
				export GROUP_ID=$(id -g)
				@docker-compose	-f	docker-compose.yml	-f	docker-compose-linux.yml	up	-d
endif

docker-stop:
ifeq	($(OS),Darwin)

				@docker-compose	stop
				@docker-sync	stop
else
				@docker-compose	stop
endif

docker-down:
	@docker-compose down --volumes
	@make -s docker-clean

docker-clean:
	@docker system prune --volumes --force
